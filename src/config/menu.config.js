import { RouterLink } from 'vue-router'
import { renderIcon } from '../utils/icon'
import { h } from 'vue'
import {
  DesktopOutline as DesktopOutlineIcon,
  FishOutline as FishOutlineIcon,
  FlaskOutline as FlaskIcon,
  InfiniteSharp as InfiniteSharpIcon,
  AnalyticsOutline as AnalyticsOutlineIcon,
  CodeSlashSharp as CodeSlashSharpIcon,
  ApertureOutline as ApertureOutlineIcon
} from '@vicons/ionicons5'

// 侧边数据
export const menuOptions = [
  {
    label: () =>
      h(
        RouterLink,
        {
          to: {
            name: 'Index'
          }
        },
        { default: () => '初始' }
      ),
    key: 'Index',
    icon: renderIcon(DesktopOutlineIcon)
  },
  {
    label: () =>
      h(
        RouterLink,
        {
          to: {
            name: 'Default'
          }
        },
        { default: () => '默认' }
      ),
    key: 'Default',
    icon: renderIcon(FishOutlineIcon)
  },
  {
    label: () =>
      h(
        RouterLink,
        {
          to: {
            name: 'Async'
          }
        },
        { default: () => '异步' }
      ),
    key: 'Async',
    icon: renderIcon(AnalyticsOutlineIcon)
  },
  {
    label: () =>
      h(
        RouterLink,
        {
          to: {
            name: 'Plugin'
          }
        },
        { default: () => 'Plugin' }
      ),
    key: 'Plugin',
    icon: renderIcon(InfiniteSharpIcon)
  },
  {
    label: () =>
      h(
        RouterLink,
        {
          to: {
            name: 'SourceCode'
          }
        },
        { default: () => '源码' }
      ),
    key: 'SourceCode',
    icon: renderIcon(CodeSlashSharpIcon)
  },
  {
    label: () =>
      h(
        RouterLink,
        {
          to: {
            name: 'Link'
          }
        },
        { default: () => '参考链接' }
      ),
    key: 'Link',
    icon: renderIcon(ApertureOutlineIcon)
  }
]