import { ref, watch } from 'vue'
import { updateStorage } from '@/utils/index'

const storageKey = 'pinia'

// 向所有的store混入一个值
const plugin = ref(0)
export const piniaPlugin = ({ store, options }) => {
  store['plugin'] = plugin
  // 对plugin数值的更改进行监听
  watch(
    () => store.plugin,
    (data) => {
      // 当 pluginStore 改变时进行更新
      if (options.id === "plugin") {
        updateStorage(storageKey, data * 10)
      }
    },
    { immediate: true, deep: true }
  )
}