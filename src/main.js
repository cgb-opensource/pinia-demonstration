import { createPinia } from 'pinia'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import naive from 'naive-ui'
import './style/index.scss'

// pinia 插件注入
import { piniaPlugin } from '@/plugins/plugin.pinia'
const pinia = createPinia()
pinia.use(piniaPlugin)

const app = createApp(App)
app.use(router)
app.use(naive)
app.use(pinia)
app.mount('#app')
