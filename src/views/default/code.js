export const storeCode = `
import { defineStore } from 'pinia'
export const defaultStore = defineStore({
  id: 'default',
  state: () => ({
    count: 0,
  })
})
`

export const storeGettersCode =`
import { defineStore } from 'pinia'
export const defaultStore = defineStore({
  id: 'default',
  state: () => ({
    count: 0,
  }),
  getters: {
    doubleCount() {
      // 可以直接通过 this 调用
      return this.count * 2
    }
  }
})
`

export const resetCode = `
const reset = () => {store.$reset()}
`

export const addCode = `
const add = () => {store.count++}
`

export const reduceCode = `
const reduce = () => {store.count--}
`

export const storeTip = `
// 需要使用 storeToRefs 来进行相应式处理
import { storeToRefs } from 'pinia'
const store = defaultStore()
const { count } = storeToRefs(store)
return { count }
`

export const useCode = `
import { defaultStore } from '../../store/default'
const store = defaultStore()
return { store }
`

export const getData = `
count: {{ store.count }}
`

export const getComputedData = `
count: {{ store.computedCount  }}
`

export const getGettersData = `
count: {{ store.count }}
count: {{ store.doubleCount }}
`

export const getDoubleCount = `
// 也可以使用计算属性做一些操作（监听属性亦如是）
const computedCount = computed(() => store.count +1 -1)
return { computedCount }

// html ${getComputedData}
`

export const changeTip = `
// 这种写法可以同时修改多个值
// 推荐
store.$patch((state) => {
  state.items.push({ name: 'shoes', quantity: 1 })
  state.hasChanged = true
})

// 不推荐，
// 任何集合修改(例如从数组中推入元素)需要创建一个新的集合
store.$patch({
  count: newCount...,
  name:  newName...,
})
`