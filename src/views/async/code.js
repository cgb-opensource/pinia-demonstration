export const storeCode = `
import { defineStore } from 'pinia'
import { photoList } from '../utils/photo'
export const asyncStore = defineStore({
  id: 'async',
  state: () => ({ imgSrc: [] }),
  actions: {
    // 设置 state
    setImgSrc (data) {
      this.imgSrc = data
    },
    // 获取图片地址
    async fetchImgSrc () {
      const list =  await photoList()
      this.setImgSrc(list)
    }
  }
})
`


export const defaultCode = `
import { asyncStore } from '@/store/async.store'
const store = asyncStore()
const imgSrc = computed(() => store.imgSrc)
`

export const fetchCode = `
const fetch = async () => {
  await store.fetchImgSrc()
}
`
export const resetCode = `
const reset = () => {store.$reset()}
`