export const createPiniaCode = `
import { ref, App, markRaw, effectScope, isVue2 } from 'vue-demi'
...

export function createPinia(): Pinia {
  const state = ref({})
  let localApp: App | undefined
  let _p: Pinia['_p'] = []
  const toBeInstalled: PiniaStorePlugin[] = []

  const pinia: Pinia = markRaw({
    install(app: App) {
      setActivePinia(pinia)
      if (!isVue2) {
        pinia._a = app
        app.provide(piniaSymbol, pinia)
        app.config.globalProperties.$pinia = pinia
        if (__DEV__ && IS_CLIENT) {
          // @ts-expect-error: weird type in devtools api
          registerPiniaDevtools(app, pinia)
        }
        toBeInstalled.forEach((plugin) => _p.push(plugin))
        toBeInstalled = []
      }
    },
    // 插件逻辑
    use(plugin) {
      if (!this._a && !isVue2) {
        toBeInstalled.push(plugin)
      } else {
        _p.push(plugin)
      }
      return this
    },

    _p,
    _a: null,
    _e: scope,
    _s: new Map<string, StoreGeneric>(),
    state,
  })

  if (__DEV__ && IS_CLIENT) {
    pinia.use(devtoolsPlugin)
  }
  return pinia
}
`

export const tip1 = `
pinia 通过vue的插件机制，暴露对外的install方法
通过provide提供pinia实例，供后续使用
暴露全局属性 $pinia
`
export const tip2 = `
可以看到 pinia 实例拥有state = ref({}) 
这其实是所有的state的集合，后面会在init的时候，
将其他模块的state挂载到pinia下
`
export const tip3 = `
pinia通过use使用插件，使用的插件会存放在实例的_p数组中，
并返回this，即当前的实例。
`