
// sessionStorage 相关
export const getStorage = (key) => {
  return JSON.parse(sessionStorage.getItem(key))
}
export const updateStorage = (key, value) => {
  return sessionStorage.setItem(
    key, JSON.stringify(value)
  )
}
